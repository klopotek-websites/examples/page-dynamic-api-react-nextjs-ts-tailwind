
# Dynamic Page: React.js + Next.js + TypeScript + Tailwind CSS

Example dynamic webiste with a navigation bar and fetching data with an API call. 
Created using:

- Next.js
- React.js
- Tailwind CSS
- TypeScript

Project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).


## Layout

### Standard size
![WebsiteLayout-1](./page-dynamic-api-react-nextjs-ts-tailwind-layout.png)

