
import Link from 'next/link'
import Image from 'next/image'

const MENU_LIST = [
    { text: "home", href: "/" },
    { text: "info", href: "/info" },
    { text: "more", href: "/more" },
    { text: "contact", href: "/contact" },

]


interface IMenuItems {
    text: string
    href: string
}

const Navbar = () => {
  return (
      <nav >
          <ul className="flex gap-2 uppercase">
              {
                  MENU_LIST.map((item, idx) => (
                      <Link key={idx} href={item.href}>
                          <li className="text-xl"> {item.text} </li>
                      </Link>
                  ))
              }
          </ul>
      </nav>
  )
}

export default Navbar;
