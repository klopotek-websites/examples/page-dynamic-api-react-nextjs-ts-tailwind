import Image from "next/image"

/**
 * 
 */
type TMemeCardProps = {
    width: number
    height: number
    url: string
}

/**
 * Meme card utilized to display the generated image
 * @param param0 
 * @returns 
 */
const MemeCard: React.FC<TMemeCardProps>  = ({width=32,height=32,url=''}: TMemeCardProps) => {
    return (
        <div className="
           ">
            {!url ?
                <div></div> : <div className="" ><Image src={url} width={width} height={height} className="mx-auto p-4 w-3/4 h-3/4 object-scale-down" alt="generated image" /></div>}
        </div>
    )
}

export default MemeCard