"use client";
import React, {useState} from 'react'
import MemeCard from '@/components/MemeCard';
import { TData, TDataItem, TImage } from '@/app/types';

type TMemeGeneratorProps = {
data: Array<TDataItem>
}

/**
 * Main component for the Meme Generator
 * @param param 
 * @returns 
 */
const MemeGenerator: React.FC<TMemeGeneratorProps> = ({
    data
}): JSX.Element  => {
  const [image, setImage] = useState<TImage>({ url: '', width: 0, height: 0 });

    function generateImage()
    {
        setImage((image)=>image=data[Math.floor(Math.random() * data.length)]);
    }

  return (
    <div className='max-w-2xl mx-auto'>
    <div className="  bg-sky-100 border-blue-300 .w-5/12 rounded flex justify-center mt-4 ">
        <button
          onClick={generateImage}
          className="font-bold 
        py-2 px-4 rounded
         bg-blue-500 
         text-white 
       hover:bg-blue-700
     "
        >
          Generate
        </button>
      </div>
      <div className=" bg-gray-100  ">
        <MemeCard
          {...image}
        />
      </div>
    </div>
  )
}

export default MemeGenerator
