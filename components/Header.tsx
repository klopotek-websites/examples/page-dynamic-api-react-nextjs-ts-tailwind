import React from 'react'
import Link from 'next/link'
import Image from 'next/image'
import Navbar from './Navbar'

const Header = () => {
    return (
        <header className="flex
         justify-between 
         align-middle 
         px-4
         m-2
         ">
            <div>
                {/* LOGO */}
                <Link href="/">
                    <Image height="80" width="100" src="/images/next.svg" alt="Logo" className="cursor-pointer" />
                </Link>
            </div>
            <Navbar />
        </header>
    )
}

export default Header