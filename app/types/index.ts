declare global {
  namespace NodeJS {
    interface ProcessEnv {
      RESOURCE_URL: string | undefined;
      THIS_ENDPOINT_DATA: string | undefined;
    }
  }
}
export {};

export type TDataItem = {
  id: string;
  name: string;
  url: string;
  width: number;
  height: number;
  box_count: number;
  captions: number;
};
export type TData = {
  data: { memes: Array<TDataItem> };
  success: boolean;
};

export type TImage = {
  url: string;
  width: number;
  height: number;
};
