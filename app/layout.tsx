import './globals.css';

import Header from '@/components/Header';

export const metadata = {
  title: 'NextJs: Dynamic App with API support',
  description: 'Created with React.js, Next.js, TypeScript and Tailwind CSS',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className="font-sans">
        <div className="max-w-5xl m-auto ">
          <Header />
          {children}
        </div>
      </body>
    </html>
  );
}
