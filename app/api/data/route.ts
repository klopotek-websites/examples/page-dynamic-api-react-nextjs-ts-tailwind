import { NextResponse, NextRequest } from 'next/server';
import axios from 'axios';

type TResponseData = any;

/**
 * Handles GET requests to /api/data?urlParam=urlParam
 * @param req
 * @returns
 */
export async function GET(
  req: NextRequest,
): Promise<NextResponse<TResponseData | null>> {
  const _endpoint = new URL(`${process.env.RESOURCE_URL}`);

  try {
    const _response = await axios.get(_endpoint.toString());
    // console.log(`GET ${_endpoint}: response:`, _response.data);
    if (_response.status == 200)
      return NextResponse.json(_response.data, {
        status: _response.data.status,
      });
  } catch (error: any) {
    console.error(`Error GET ${_endpoint}:`, error.response.data.message);
    return NextResponse.json(error.response.data.message, {
      status: error.response.data.statusCode,
    });
  }

  return NextResponse.json(null, { status: 500 });
}
