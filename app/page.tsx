import getData from './actions/getData';
import MemeGenerator from '@/components/MemeGenerator';

export default async function Home() {
  const data = await getData();
  // console.log('Home: data: ', data?.data.memes);

  if (!data?.success) throw new Error('Could not fetch data');

  return (
    <main className="border-4 m-10 border-sky-200">
      <h1 className="text-3xl font-bold text-center">Meme Generator</h1>
      <section>
        <MemeGenerator data={data?.data.memes} />
      </section>
    </main>
  );
}
