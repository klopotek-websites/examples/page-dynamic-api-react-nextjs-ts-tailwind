import axios from 'axios';
import { TData } from '../types';
import { THIS_ENDPOINT_DATA } from '../common';

/**
 * Function to retrieve data by calling the THIS_ENDPOINT_DATA endpoint
 * @returns TData
 */
async function getData(): Promise<TData | null> {
  const _endpoint = `${process.env.NEXTAUTH_URL}${THIS_ENDPOINT_DATA}`;
  // console.log(`getData: ${_endpoint}`);

  try {
    const _response = await axios.get(_endpoint);
    // console.log(_response.data);
    if (_response.status == 200) return _response.data;
    return null;
  } catch (error: any) {
    console.log('fetchData error: ', error.response.data.message);
    return null;
  }
}

export default getData;
