### Changelog

All notable changes to this project will be documented in this file. Dates are displayed in UTC.

Generated by [`auto-changelog`](https://github.com/CookPete/auto-changelog).

#### [v0.2.0](https://gitlab.com/klopotek-websites/examples/page-dynamic-api-react-nextjs-ts-tailwind/compare/v0.1.0...v0.2.0)

> 15 September 2023

- Refactored the Home component to fetch data from the backend (/api/data),... [`#1`](https://gitlab.com/klopotek-websites/examples/page-dynamic-api-react-nextjs-ts-tailwind/merge_requests/1)
- Refactored the main component so that the meme data is retrieved from the back-end [`02f77e4`](https://gitlab.com/klopotek-websites/examples/page-dynamic-api-react-nextjs-ts-tailwind/commit/02f77e462705c7cb739a37f21e2a675cf3ed1df9)
- Added CHANGELOG.md [`be68a98`](https://gitlab.com/klopotek-websites/examples/page-dynamic-api-react-nextjs-ts-tailwind/commit/be68a98537fcfa800676febdbbbb38a5c93c9a8e)

#### [v0.1.0](https://gitlab.com/klopotek-websites/examples/page-dynamic-api-react-nextjs-ts-tailwind/compare/v0.0.1...v0.1.0)

> 24 June 2023

- Add LICENSE [`ed8b0e0`](https://gitlab.com/klopotek-websites/examples/page-dynamic-api-react-nextjs-ts-tailwind/commit/ed8b0e0e40aaad1d17d072f569f50182fb32fd10)
- First version of the website with a meme generator [`754b5b5`](https://gitlab.com/klopotek-websites/examples/page-dynamic-api-react-nextjs-ts-tailwind/commit/754b5b59851ce194eacd548368fc075c58b510e0)
- Initial cleanup [`c897b18`](https://gitlab.com/klopotek-websites/examples/page-dynamic-api-react-nextjs-ts-tailwind/commit/c897b186dea751bede79a0ea172b9b70b3989d0e)

#### v0.0.1

> 24 June 2023

- Initial commit from Create Next App [`f69b509`](https://gitlab.com/klopotek-websites/examples/page-dynamic-api-react-nextjs-ts-tailwind/commit/f69b50905c65bd59a34031f1176ba64bf3b88a7a)
